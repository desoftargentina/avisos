import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LayoutComponent } from './layout.component';

@NgModule({
  imports: [
    BrowserAnimationsModule
  ],
  providers: [],
  declarations: [LayoutComponent],
  bootstrap: [LayoutComponent]
})
export class LayoutModule {
}
